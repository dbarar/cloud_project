import sqlite3
import json
import sys
import base64
from flask import Flask, request, make_response
import hashlib
import codecs

app = Flask(__name__)
cursor = None


def get_hash(buf):
    hasher = hashlib.md5()
    hasher.update(buf)
    return hasher.hexdigest()


def base64_to_string(buf):
    return base64.b64decode(buf)


@app.route('/dbarar/<service_name>/<name_file>', methods=['GET', 'POST'])
def download(service_name, name_file):
    global cursor
    global conn
    headers = {"Content-Disposition": "attachment; filename=%s" % name_file}

    cursor.execute('SELECT* FROM service_table WHERE service_name = (?) AND name_file = (?)',
                   [service_name, name_file])
    data_found = cursor.fetchall()

    if not data_found:
        response = {"Status": '1',
                    "Message": "File not found"}
        cod_error = 400
        response_json = json.dumps(response)
        return response_json, cod_error
    else:
        file_content = base64_to_string(data_found[0][3])
        body = codecs.escape_decode(file_content)[0]
        body = body[2:len(body)-1]
        return make_response((body, headers))


def op_details(data):
    global cursor
    global conn
    response = {}
    service_name = data["Service"]
    name_file = data["Parameters"]["Name"]

    cursor.execute('SELECT* FROM service_table WHERE service_name = (?) AND name_file = (?)',
                   [service_name, name_file])
    data_found = cursor.fetchall()
    if data_found:
        service_name = data_found[0][0]
        name_file = data_found[0][1]
        hash_file = data_found[0][2]
        content = data_found[0][3]
        metadata = data_found[0][4]

        response["Status"] = '0'
        response["Message"] = "File found"
        response["Name"] = name_file
        response["Hash"] = hash_file
        response["DownloadUrl"] = "http://127.0.0.1:5000/dbarar/" + service_name + "/" + name_file
        response["Size"] = len(content)
        response["Metadata"] = metadata
        response["Content"] = content
        cod_error = 200
    else:
        response["Status"] = '-1'
        response["Message"] = "File not found"
        cod_error = 404

    return response, cod_error


def op_add(data):
    global cursor
    global conn
    response = {}
    service_name = data["Service"]
    name_file = data["Parameters"]["Name"]
    hash_file = data["Parameters"]["Hash"]
    content_file = data["Parameters"]["Content"]

    metadata_file = "nedefinit"

    if "Metadata" in data["Parameters"]:
        metadata_file = data["Parameters"]["Metadata"]

    if get_hash(base64_to_string(content_file)) == hash_file:
        cursor.execute('INSERT INTO service_table VALUES (?, ?, ?, ?, ?)',
                       [service_name, name_file, hash_file, content_file, metadata_file])
        conn.commit()
        response["Status"] = '0'
        response["Message"] = "File submitted and stored successfully"
        cod_error = 200
    else:
        response["Status"] = '-1'
        response["Message"] = "Hashes not matching, servers - side vs client - side"
        cod_error = 403

    return response, cod_error


def op_edit(data):
    global cursor
    global conn
    response = {}
    metadata_to_edit = "procesat"
    service_name = data["Service"]
    name_file = data["Parameters"]["Name"]

    if "Metadata" in data["Parameters"]:
        metadata_to_edit = str(data["Parameters"]["Metadata"])

    cursor.execute('UPDATE service_table \
                    SET metadata = (?) \
                    WHERE service_name = (?) AND name_file = (?)',
                   [metadata_to_edit, service_name, name_file])
    conn.commit()

    response["Status"] = '0'
    response["Message"] = "Statistics queried successfully"
    cod_error = 200

    return response, cod_error


def op_list(data):
    global cursor
    global conn
    response = {}
    limit = None
    sort = "asc"
    details = True
    service_name = data["Service"]

    if "Limit" in data["Parameters"]:
        limit = int(data["Parameters"]["Limit"])
    if "Sort" in data["Parameters"]:
        sort = data["Parameters"]["Sort"]
    if "Details" in data["Parameters"]:
        details = bool(data["Parameters"]["Details"])

    cursor.execute('SELECT name_file, hash_file, metadata, content FROM service_table WHERE service_name = (?)',
                   [service_name])
    data_found = cursor.fetchall()

    if sort == "asc":
        data_found = sorted(data_found, key=lambda x: x[1])
    elif sort == "desc":
        data_found = sorted(data_found, key=lambda x: x[1], reverse=True)

    # if not details:
    #     data_found = [i[0] for i in data_found]

    list_data = []
    for item in data_found[0:limit]:
        list_data_item = {}
        list_data_item["Name"] = item[0]
        list_data_item["Hash"] = item[1]
        list_data_item["Size"] = len(item[3])
        list_data_item["Metadata"] = item[2]
        list_data_item["DownloadUrl"] = "http://127.0.0.1:5000/dbarar/" + service_name + "/" + item[0]
        list_data.append(list_data_item)

    response["Status"] = '0'
    response["Message"] = "File listed successfully"
    response["Count"] = len(data_found)
    response["List"] = list_data
    cod_error = 200

    return response, cod_error


def op_remove(data):
    global cursor
    global conn
    response = {}
    service_name = data["Service"]
    name_file = data["Parameters"]["Name"]

    cursor.execute('DELETE FROM service_table WHERE service_name = (?) and name_file = (?)',
                   [service_name, name_file])
    conn.commit()

    response["Status"] = '0'
    response["Message"] = "File removed successfully"
    cod_error = 200

    return response, cod_error


def op_stats(data):
    global cursor
    global conn
    response = {}
    service_name = data["Service"]

    cursor.execute('SELECT COUNT(*) FROM service_table WHERE service_name = (?)',
                   [service_name])
    file_count = cursor.fetchall()

    response["Status"] = '0'
    response["Message"] = "Statistics queried successfully"
    response["FileCount"] = file_count
    cod_error = 200

    return response, cod_error


@app.route('/dbarar/', methods=['POST', 'GET'])
def init():
    cod_error = 200
    response = {"Status": '0',
                "Message": "Ok"}

    if request.method == 'POST':
        data = request.get_json()
        print(data)
        try:
            if data["Operation"] == "details":
                response, cod_error = op_details(data)
            elif data["Operation"] == "add":
                response, cod_error = op_add(data)
            elif data["Operation"] == "edit":
                response, cod_error = op_edit(data)
            elif data["Operation"] == "list":
                response, cod_error = op_list(data)
            elif data["Operation"] == "remove":
                response, cod_error = op_remove(data)
            elif data["Operation"] == "stats":
                response, cod_error = op_stats(data)
            else:
                response = "Operation type not found"
                cod_error = 404
        except Exception as e:
            exception = str(e)

            if exception == "'Operation'":
                exception = "Bad request, operation parameter is missing"
            if exception == "'Service'":
                exception = "Bad request, service parameter is missing"
            if exception == "'Parameters'":
                exception = "Bad request, Parameters parameter is missing"
            if exception == "'Hash'":
                exception = "Bad request, Hash parameter is missing"
            if exception == "'Metadata'":
                exception = "Bad request, Metadata parameter is missing"
            if exception == "'Content'":
                exception = "Bad request, Content parameter is missing"

            if exception == "UNIQUE constraint failed: service_table.name_file, service_table.service_name":
                exception = "Duplication of data not allowed"
            response["Status"] = '-1'
            response["Message"] = exception
            cod_error = 400

    response_json = json.dumps(response)
    return response_json, cod_error


def create_table():
    global cursor
    global conn
    try:
        cursor.execute(''' CREATE TABLE IF NOT EXISTS service_table
                                        (service_name VARCHAR(255), \
                                        name_file VARCHAR(33) NOT NULL,\
                                        hash_file VARCHAR(33) NOT NULL,\
                                        content VARCHAR(2048), \
                                        metadata VARCHAR(50), \
                                        PRIMARY KEY(name_file, service_name)) ''')
        conn.commit()
    except sqlite3.Error as e:
        print(e)
        return False

    return True


if __name__ == '__main__':
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    if not create_table():
        print("create_table failed")
        sys.exit(1)
    app.run()
    app.run(host='0.0.0.0')
