import sys
import sqlite3
from functools import wraps
from passlib.hash import sha256_crypt
from flask import Flask, url_for, render_template, request, flash, redirect, session, redirect
from wtforms import Form, StringField, TextAreaField, PasswordField, validators

import time
from service_lib import ServiceHandler
import random
import string
import json
import base64
import codecs

import datetime
import os

app = Flask(__name__)

class CloudFile:
    def __init__(self, inputFile=None, filename=None):
        self.file = inputFile
        self.filename = inputFile.filename if inputFile else filename
        self.data = inputFile.read() if inputFile else None
        self.content = str(self.data).encode('utf-8') if self.data else None
        self.hash_content = None
        self.ext = os.path.splitext(self.filename)[1]
        self.availableExtensions = {".jpg", ".png", ".gif", ".bmp", ".pdf", ".mp4", ".mpg", ".avi"} #etc

    def upload(self, username, service):
        self.hash_content = service.add(username, self.filename, self.content, json.dumps({'data': time.strftime("%c"), 'count': 0}))

    def share(self, username, personToShare, service):
        service.share_file(username, personToShare, self.filename)

    def delete(self, username, service):
        service.remove(username, self.filename)

class User:
    def __init__(self, name=None, username=None, email=None, password=None, register_date=datetime.datetime.now()):
        self.username = username

        self.conn = sqlite3.connect('database.db')
        self.cursor = self.conn.cursor()

        self.name = name if name else self.searchField('name')
        self.email = email if email else self.searchField('email')
        self.password = password if password else self.searchField('password')
        self.register_date = register_date

        self.service = ServiceHandler(service_url="http://127.0.0.1:5000/dbarar/", service_name=self.username)
        self.files = self.service.list(username)['List']

    def getName(self):
        return self.name

    def getUsername(self):
        return self.username

    def getEmail(self):
        return self.email

    def getPassword(self):
        return self.password

    def register(self):
        if self.verifyData():
            self.cursor.execute('INSERT INTO users(name, email, username, password) VALUES(?, ?, ?, ?)',
                           [self.name, self.email, self.username, self.password])
            self.conn.commit()
            return True
        else:
            return False

    def verifyData(self):
        self.cursor.execute('SELECT username, email FROM users WHERE username = (?) AND email = (?)', [self.username, self.email])
        if self.cursor.fetchall() != []:
            return False
        else :
            return True

    def searchField(self, field):
        self.cursor.execute('SELECT (?) FROM users WHERE username = (?)', [field, self.username])
        return self.cursor.fetchone()

    def upload(self, inputFile):
        if inputFile and inputFile.content:
            # inputFile.hash_content = s.add(self.username, input.filename, str(data).encode('utf-8'), json.dumps({'data': time.strftime("%c"), 'count': 0}))
            inputFile.upload(self.username, self.service)
        else:
            return 1
        if inputFile.hash_content != None:
            self.files.append
            return 0
        else:
            return -1

    def share(self, inputFile, personToShare):
        inputFile.share(self.username, personToShare, self.service)

    def delete(self, inputFile):
        inputFile.delete(self.username, self.service)


# Check if user logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap

# Check if there is not logged in users
def is_logged_out(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if not 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('personal_storage', username=session['username']))
    return wrap

# Home page
@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method != 'POST':
        error = 'Not a POST method'
    if not session.get('logged_in'):
        return render_template('home.php')
    error = 'You are logged in already!'
    return redirect(url_for('personal_storage', username=session['username'], error=error))

# Register Form Class
class RegisterForm(Form):
    name = StringField('Name', [validators.Length(min=4, max=50)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm Password')


# User Register
@app.route('/register', methods=['GET', 'POST'])
def register():
    if session.get('logged_in'):
        return redirect(url_for('index'))
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User(form.name.data, form.username.data, form.email.data, sha256_crypt.hash(form.password.data), datetime.datetime.now())
        if user.register():
            flash('User successfully registered', 'success')
            return redirect(url_for('login'))
        else:
            flash('Username or Email already exists', 'danger')
            return redirect(url_for('register'))

    return render_template('register.php', form=form)


@app.route('/login', methods=['GET', 'POST'])
@is_logged_out
def login():
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    # global conn
    # global cursor
    if request.method == 'POST':
        username = request.form['username']
        password_candidate = request.form['password']

        # Get user by username
        result = cursor.execute('SELECT * FROM users WHERE username = (?)', [username])
        # Get stored hash
        data = cursor.fetchone()
        # Close Connection
        cursor.close()

        if data != None:
            user = User(name=data[0], username=data[1], email=data[2], password=data[3], register_date=data[4])
            # Check password
            if sha256_crypt.verify(password_candidate, user.getPassword()):
                # Passed
                session['logged_in'] = True
                session['username'] = username

                flash('You are now logged in', 'success')
                return redirect(url_for('personal_storage', username=session['username']))
            else:
                flash('Invalid Username or Password', 'danger')
                return render_template('login.php')
        else:
            flash('Username not found')
            return redirect(url_for('login'))
    return render_template('login.php')

@app.route('/personal_storage/<username>/', methods=['GET', 'POST'])
@is_logged_in
def personal_storage(username):
    user = User(username=username)

    if request.method == 'POST':
        for fileInfo in user.files:
            dt = request.form.get(fileInfo['Name'])
            if dt:
                user.delete(CloudFile(filename=fileInfo['Name']))
                flash('File deleted', 'success')
                return redirect(url_for('personal_storage', username=username, result=user.files))

        for fileInfo in user.files:
            shareOn = request.form.get(fileInfo['Name']+'Share')
            shareUser = request.form.get(fileInfo['Name']+'Username')
            print(shareOn)
            print(shareUser)
            if shareOn and shareUser:
                # s.share_file(username, shareUser, fileInfo['Name'])
                flash('File shared', 'success')
                user.share(CloudFile(filename=fileInfo['Name']), shareUser)
                return redirect(url_for('personal_storage', username=username, result=user.files))

        file = request.files.get('file')
        if file:
            result = user.upload(CloudFile(inputFile=file))
            if result == 0:
                flash('File successfully uploaded', 'success')
            elif result == 1:
                flash('Select a file', 'danger')
            else:
                flash('File can not be uploaded', 'danger')
        return redirect(url_for('personal_storage', username=username, result=user.files))
    return render_template('personal_storage.php', username = username, result=user.files)

@app.route('/personal_storage/<username>/<filename>', methods=['GET', 'POST'])
@is_logged_in
def personal_storage_view_file(username, filename):
    extensions = {".jpg", ".png", ".gif", ".bmp", ".pdf", ".mp4", ".mpg", ".avi"} #etc
    s = ServiceHandler(service_url="http://127.0.0.1:5000/dbarar/", service_name=username)
    file_details = s.details(username, filename)
    file_details = base64.b64decode(file_details['Content'])
    content = codecs.escape_decode(file_details)[0]
    if any(filename.endswith(ext) for ext in extensions):
        # print('type', type(file_details))
        # print('file_details = ', content[2:len(content)-1])
        content = str(base64.b64encode(content[2:len(content)-1]))
        content = content[2:len(content)-1]
        # print('result = ', content)
        return render_template('personal_storage_view_file.php', username = username, filename=filename, result = content)
    content = file_details[2:len(file_details)-1].decode('unicode_escape')
    return render_template('personal_storage_view_file.php', username = username, filename=filename, result = content)

# Logout
@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash('You are now logged out', 'success')
    print('Logout ', session)
    return redirect(url_for('login'))

def create_users_table():
    global conn
    global cursor
    try:
        cursor.execute(''' CREATE TABLE IF NOT EXISTS users(name VARCHAR(100), username VARCHAR(30) PRIMARY KEY, email VARCHAR(100), password  VARCHAR(100), register_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ''')
        conn.commit()
    except sqlite3.Error as e:
        print(e)
        return False

    return True

if __name__ ==  "__main__":
    conn = sqlite3.connect('database.db')
    cursor = conn.cursor()
    if not create_users_table():
        print("create_user_table failed")
        sys.exit(1)
    else:
        print("Users created")


    # Secret key for session
    app.secret_key='secret123'
    # session.clear()
    # app.run(debug=True)
    app.run(host = '0.0.0.0', port = 5001, debug=True)
