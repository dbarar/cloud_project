{% extends 'session_layout.php' %}

{% block body %}
    <h3><b>Welcome {{ username }}</h3>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            {%  if  filename.endswith(".jpg") or
                    filename.endswith(".png") or
                    filename.endswith(".bmp") or
                    filename.endswith(".gif") %}
                <div class="container">
                    <img src="data:image/;base64, {{result}}" class="img-responsive" />
                    <br>
                </div>
            {% elif filename.endswith(".mp4") %}
                <div class="container">
                    <video controls>
                        <source type="video/mp4" src="data:video/mp4;base64, {{result}}">
                    </video>
                </div>
            {% elif filename.endswith(".pdf") %}
                <div>
                    <iframe src="data:application/pdf;base64, {{result}}" width="75%" height="75%"></iframe>
                </div>
            {% else %}
                <div ng-app="MyApp" ng-controller="AppCtrl">
                  <!-- HTML Box -->
                  <div class="codeheader" id="codeheader_html">{{ html }}</div>
                  <div id="codebox">
                    <pre><code data-language="html">
                    <p>{{ result }}</p>
                    </code></pre>
                  </div>
                </div>
          {% endif %}
      </div>
    </div>

</div>

{% endblock %}
