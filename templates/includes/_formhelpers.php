{% macro render_field(field) %}
  <!-- {{ field.label }} -->
  <!-- {{ field(**kwargs)|safe }} -->
  {% if field.errors %}
    {% for error in field.errors %}
      <div class="alert alert-danger">
        <strong>{{ error }}</strong>
      </div>
    {% endfor %}
  {% endif %}
{% endmacro %}
