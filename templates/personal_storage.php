{% extends 'session_layout.php' %}

{% block body %}
    <h3><b>Welcome {{ username }}</h3>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <div class="container">
                {% with messages = get_flashed_messages() %}
                    {% if messages %}
                        <ul class=flashes>
                        {% for message in messages %}
                        <li>{{ message }}</li>
                        {% endfor %}
                        </ul>
                    {% endif %}
                {% endwith %}

                <form action="{{url_for('personal_storage', username=username, result=result)}}" method="POST">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Size</th>
                            <th>Download</th>
                            <th>Delete</th>
                            <th>Share</th>
                          </tr>
                        </thead>
                        <tbody>
                            {% for file in result %}
                                <tr>
                                  <td><a href={{ file['Name'] }}>{{ file['Name'] }}</td>
                                  <td>{{ file['Size'] }}</td>
                                  <td align="center"><a href={{file['DownloadUrl']}}><button type="button" class="btn btn-success" ><span class="input-group-addon"><i class="glyphicon glyphicon-download"></i></span></button></a></td>
                                  <td align="center"><button type="submit" class="btn btn-danger" name="{{file['Name']}}" value="Delete"><span class="input-group-addon"><i class="glyphicon glyphicon-trash"></i></span></button></td>
                                  <td><button type="submit" name="{{file['Name']}}Share" class="btn btn-primary" value="Share"><span class="input-group-addon"><i class="glyphicon glyphicon-share"></i></span></button><input type="text" class="form-control" name="{{file['Name']}}Username" placeholder="Share Username" style="width: 50%;"></td>
                                  <!-- <td><input type="submit" name="{{file['']}}" class="icon icon-search" value="Submit"></td> -->
                                </tr>
                                <!-- <li>{{ file['Name'] }} {{ file['Size'] }} {{ file['DownloadUrl'] }}</li> -->
                            {% endfor %}
                        </tbody>
                    </table>
                    <!-- <input type="submit" method="GET"> -->
                </form>
            </div>

        </div>
        <!-- <div id="download" class="tab-pane fade">
        <h3>Download</h3>
        </div> -->
        <div id="upload" class="tab-pane fade">
            <h3>Upload</h3>
            <form method="post" enctype="multipart/form-data">
                Select file to upload:
                <input type="file" name="file" id="file">
                <input type="submit" value="Upload" name="submit">
            </form>
        </div>
    </div>



    <!-- <div class="container">
        <h1><b>{{ 'Welcome' }}
            <b>{{ username }}
        </h1>

        {% with messages = get_flashed_messages() %}
            {% if messages %}
                <ul class=flashes>
                {% for message in messages %}
                <li>{{ message }}</li>
                {% endfor %}
                </ul>
            {% endif %}
        {% endwith %}

    </div> -->

    <!-- <form method="post" enctype="multipart/form-data">
        <input type="file" name="file" id="file">
        <input type="submit" value="Upload Image" name="submit">
    </form> -->

{% endblock %}
