
{% extends 'layout.php' %}

  {% block body %}
  <div>
      <h3>Login<h3>
  </div>

  {% with messages = get_flashed_messages() %}
      {% if messages %}
          <ul class=flashes>
          {% for message in messages %}
          <li>{{ message }}</li>
          {% endfor %}
          </ul>
      {% endif %}
  {% endwith %}

  <form method = "POST", action = "">
    <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
      <input id="username" type="text" class="form-control" name="username" placeholder="Username">
    </div>
    <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
      <input id="password" type="password" class="form-control" name="password" placeholder="Password">
    </div>
    <br>
    <button type="submit">Login</button>
  </form>
  {% endblock %}
