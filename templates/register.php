{% extends 'layout.php' %}

  {% block body %}
  <div>
      <h3>Register<h3>
  </div>

  {% with messages = get_flashed_messages() %}
      {% if messages %}
          <ul class=flashes>
          {% for message in messages %}
          <div class="alert alert-danger">
              <strong><li>{{ message }}</li></strong>
          </div>
          {% endfor %}
          </ul>
      {% endif %}
  {% endwith %}

  {% from "includes/_formhelpers.php" import render_field %}
  <!-- <b>Name<br> -->
  <form method="POST" action="">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input id="name" type="text" class="form-control" name="name" placeholder="Enter Name">
    </div>
    {{render_field(form.name, class_="form-control")}}
    <br>

    <!-- <b>Username<br> -->
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input id="username" type="text" class="form-control" name="username" placeholder="Enter Username">
    </div>
    {{render_field(form.username, class_="form-control")}}
    <br>

    <!-- <b>Email<br> -->
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <input id="email" type="text" class="form-control" name="email" placeholder="Enter Email">
    </div>
    {{render_field(form.email, class_="form-control")}}
    <br>

    <!-- <b>Password<br> -->
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input id="password" type="password" class="form-control" name="password" placeholder="Enter Password">
    </div>
    {{render_field(form.password, class_="form-control")}}
    <br>

    <!-- <b>Confirm<br> -->
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <input id="confirm" type="password" class="form-control" name="confirm" placeholder="Confirm Password">
    </div>
    {{render_field(form.confirm, class_="form-control")}}
    <br>
    <button type="submit">Register</button>
  </form>
  {% endblock %}
