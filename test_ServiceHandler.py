from unittest import TestCase
from service_lib import ServiceHandler
import random
import string
import sqlite3
import json
import os


class TestServiceHandler(TestCase):
    MIN_DIM = 10
    MAX_DIM = 1024

    def setUp(self):
        self.service_handler = ServiceHandler(service_url="http://127.0.0.1:5000/dbarar/", service_name="denisa_test")
        self.connection = sqlite3.connect(os.path.join(os.getcwd(), "database.db"))
        self.cursor = self.connection.cursor()
        self.service_name_test_01 = "denisa_test_01"
        self.service_name_test_02 = "denisa_test_02"

    def create_content(self):
        length = random.randint(self.MIN_DIM, self.MAX_DIM)
        letters = string.ascii_lowercase
        return str(''.join(random.choice(letters) for i in range(length))).encode('utf-8')

    def verify_data_in_db(self, service_name, name_file):
        self.cursor.execute('SELECT * FROM service_table WHERE service_name = (?) AND name_file = (?)',
                            [service_name, name_file])
        return self.cursor.fetchall()

    def verify_metadata_in_db(self, service_name, name_file, metadata):
        self.cursor.execute('SELECT metadata FROM service_table WHERE service_name = (?) AND name_file = (?)',
                            [service_name, name_file])
        return metadata in str(self.cursor.fetchall())

    def remove_from_db(self, service_name, name_file):
        self.cursor.execute('DELETE FROM service_table WHERE service_name = (?) AND name_file = (?)',
                            [service_name, name_file])
        self.connection.commit()

    def clean_db(self, service_name):
        self.cursor.execute('DELETE FROM service_table WHERE service_name = (?)',
                            [service_name])
        self.connection.commit()

    def test_get_base64(self):
        self.assertTrue(isinstance(ServiceHandler.string_to_base64(self.create_content()), str))

    def test_get_hash(self):
        self.assertTrue(isinstance(ServiceHandler.get_hash(self.create_content()), str))

    def test_add(self):
        name_file = self.service_handler.add(self.service_name_test_01,
                                             None,
                                             self.create_content(),
                                             json.dumps({'data': 'azi', 'count': 109}))
        self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
        self.remove_from_db(self.service_name_test_01, name_file)

    def test_edit(self):
        name_file = self.service_handler.add(self.service_name_test_01,
                                             None,
                                             self.create_content(),
                                             json.dumps({'data': 'azi', 'count': 109}))
        self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
        self.service_handler.edit(self.service_name_test_01, name_file,
                                  metadata=json.dumps({'data': 'after', 'count': 10}))
        self.assertTrue(self.verify_metadata_in_db(self.service_name_test_01, name_file,
                                                   metadata=json.dumps({'data': 'after', 'count': 10})))
        self.remove_from_db(self.service_name_test_01, name_file)

    def test_remove(self):
        name_file = self.service_handler.add(self.service_name_test_01,
                                             None,
                                             self.create_content(),
                                             json.dumps({'data': 'azi', 'count': 109}))
        self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
        self.service_handler.remove(self.service_name_test_01, name_file)
        self.assertFalse(self.verify_data_in_db(self.service_name_test_01, name_file))

    def test_details(self):
        content = self.create_content()
        metadata = json.dumps({'data': 'details', 'count': 100})
        name_file = self.service_handler.add(self.service_name_test_01,
                                             None,
                                             content,
                                             metadata)
        self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
        response_details = self.service_handler.details(self.service_name_test_01, name_file)
        self.assertIsNotNone(response_details)
        self.assertEqual(response_details['Name'], name_file)
        self.assertEqual(response_details['Size'], len(self.service_handler.string_to_base64(content)))
        self.assertEqual(response_details['Metadata'], metadata)
        self.assertIsNotNone(response_details['DownloadUrl'])
        self.remove_from_db(self.service_name_test_01, name_file)

    def test_list(self):
        list_name = []
        size_list = 5
        for i in range(size_list):
            name_file = self.service_handler.add(self.service_name_test_01,
                                                 None,
                                                 self.create_content(),
                                                 json.dumps({'data': 'azi', 'count': 109}))
            self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
            list_name.append(name_file)
        response = self.service_handler.list(self.service_name_test_01)
        self.assertEqual(response['Count'], size_list)
        list_response_name = []
        for item in response['List']:
            list_response_name.append(item['Name'])
        self.assertEqual(list_response_name.sort(), list_name.sort())
        for list_name in list_name:
            self.remove_from_db(self.service_name_test_01, list_name)

    def test_stats(self):
        list_name = []
        size_list = 10
        for i in range(size_list):
            name_file = self.service_handler.add(self.service_name_test_01,
                                                 None,
                                                 self.create_content(),
                                                 json.dumps({'data': 'azi', 'count': 109}))
            self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
            list_name.append(name_file)
        response = self.service_handler.stats(self.service_name_test_01)
        self.assertEqual(response['FileCount'][0][0], size_list)
        for name_file in list_name:
            self.remove_from_db(self.service_name_test_01, name_file)

    def test_share_file(self):
        name_file = self.service_handler.add(self.service_name_test_01,
                                             None,
                                             self.create_content(),
                                             json.dumps({'data': 'azi', 'count': 109}))
        self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
        self.service_handler.share_file(self.service_name_test_01, self.service_name_test_02, name_file)
        self.assertTrue(self.verify_data_in_db(self.service_name_test_02, name_file))
        self.remove_from_db(self.service_name_test_01, name_file)
        self.remove_from_db(self.service_name_test_02, name_file)

    def test_unshare_file(self):
        name_file = self.service_handler.add(self.service_name_test_01,
                                             None,
                                             self.create_content(),
                                             json.dumps({'data': 'azi', 'count': 109}))
        self.assertTrue(self.verify_data_in_db(self.service_name_test_01, name_file))
        self.service_handler.share_file(self.service_name_test_01, self.service_name_test_02, name_file)
        self.assertTrue(self.verify_data_in_db(self.service_name_test_02, name_file))
        self.service_handler.unshare_file(self.service_name_test_01, self.service_name_test_02, name_file)
        self.assertFalse(self.verify_data_in_db(self.service_name_test_02, name_file))
        self.remove_from_db(self.service_name_test_01, name_file)
        self.remove_from_db(self.service_name_test_02, name_file)

    def tearDown(self):
        self.clean_db(self.service_name_test_01)
        self.clean_db(self.service_name_test_02)
        pass
