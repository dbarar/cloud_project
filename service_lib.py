import urllib.request
import ast
import logging
import base64
import hashlib
import json


class ServiceHandler:

    def __init__(self, service_url, service_name):
        self.service_url = service_url
        self.service_name = service_name

    @staticmethod
    def string_to_base64(buf):
        return base64.b64encode(buf).decode('ascii')

    @staticmethod
    def get_hash(buf):
        hasher = hashlib.md5()
        hasher.update(buf)
        return hasher.hexdigest()

    @staticmethod
    def base64_to_string(buf):
        return base64.b64decode(buf)

    def send_request(self, json_data):
        try:
            req = urllib.request.Request(url=self.service_url, data=json_data, method='POST')
            req.add_header('Content-Type', 'application/json')
            with urllib.request.urlopen(req) as f:
                body_response = f.read().decode('utf8')
                print(body_response)
            return ast.literal_eval(body_response)
        except Exception as e:
            logging.error(e)

    @staticmethod
    def make_json_data(operation, service, parameters):
        return json.dumps(
            {"Operation": operation,
             "Service": service,
             "Parameters": parameters}, ensure_ascii=False).encode('utf-8')

    def add(self, service_name, name_file, content, metadata):
        hash_content = self.get_hash(content)
        if name_file is None:
            name_file = hash_content + ".txt"
        parameters = {"Name": name_file,
                      "Hash": hash_content,
                      "Content": self.string_to_base64(content),
                      "Metadata": metadata}
        json_data = self.make_json_data("add", service_name, parameters)
        self.send_request(json_data)
        return name_file

    def edit(self, service_name, name_file, metadata):
        parameters = {"Name": name_file,
                      "Metadata": metadata}
        json_data = self.make_json_data("edit", service_name, parameters)
        self.send_request(json_data)

    def remove(self, service_name, name_file):
        parameters = {"Name": name_file}
        json_data = self.make_json_data("remove", service_name, parameters)
        self.send_request(json_data)

    def details(self, service_name, name_file):
        parameters = {"Name": name_file}
        json_data = self.make_json_data("details", service_name, parameters)
        return self.send_request(json_data)

    def list(self, service_name):
        parameters = {}
        json_data = self.make_json_data("list", service_name, parameters)
        return self.send_request(json_data)

    def stats(self, service_name):
        json_data = self.make_json_data("stats", service_name, {})
        return self.send_request(json_data)

    def share_file(self, service_name, service_name_to_share, name_file):
        response_details = self.details(service_name, name_file)
        self.add(service_name_to_share,
                 name_file,
                 self.base64_to_string(response_details['Content']),
                 response_details['Metadata'])

    def unshare_file(self, service_name, service_name_to_share, name_file):
        self.remove(service_name_to_share, name_file)
